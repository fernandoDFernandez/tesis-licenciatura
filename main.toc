\babel@toc {spanish}{}
\contentsline {chapter}{Resumen}{\es@scroman {ii}}{Doc-Start}
\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}El problema de valores iniciales en ecuaciones diferenciales ordinarias}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Introducci\IeC {\'o}n}{5}{section.2.1}
\contentsline {section}{\numberline {2.2}Teor\IeC {\'\i }a}{6}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Ejemplos}{7}{subsection.2.2.1}
\contentsline {section}{\numberline {2.3}Problemas bien planteados}{10}{section.2.3}
\contentsline {chapter}{\numberline {3}M\IeC {\'e}todos num\IeC {\'e}ricos de un paso}{14}{chapter.3}
\contentsline {section}{\numberline {3.1}Introducci\IeC {\'o}n}{14}{section.3.1}
\contentsline {section}{\numberline {3.2}Error global y error local}{17}{section.3.2}
\contentsline {section}{\numberline {3.3}Propiedades importantes de los m\IeC {\'e}todos}{18}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Criterio para analizar los m\IeC {\'e}todos}{18}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Definiciones}{18}{subsection.3.3.2}
\contentsline {section}{\numberline {3.4}M\IeC {\'e}todos num\IeC {\'e}ricos de un paso}{19}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Estabilidad Num\IeC {\'e}rica}{20}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}A-estabilidad}{22}{subsection.3.4.2}
\contentsline {section}{\numberline {3.5}M\IeC {\'e}todos de Taylor}{24}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}Un m\IeC {\'e}todo de orden dos}{27}{subsection.3.5.1}
\contentsline {subsection}{\numberline {3.5.2}A-Estabilidad }{28}{subsection.3.5.2}
\contentsline {section}{\numberline {3.6}M\IeC {\'e}todos de Euler}{33}{section.3.6}
\contentsline {subsection}{\numberline {3.6.1}M\IeC {\'e}todo de Euler expl\IeC {\'\i }cito}{33}{subsection.3.6.1}
\contentsline {subsection}{\numberline {3.6.2}M\IeC {\'e}todo de Euler impl\IeC {\'\i }cito}{37}{subsection.3.6.2}
\contentsline {section}{\numberline {3.7}M\IeC {\'e}todos de Runge-Kutta}{42}{section.3.7}
\contentsline {subsection}{\numberline {3.7.1}Un m\IeC {\'e}todo de RK de orden dos}{44}{subsection.3.7.1}
\contentsline {subsection}{\numberline {3.7.2}Cero estabilidad}{47}{subsection.3.7.2}
\contentsline {chapter}{\numberline {4}Desarrollo de aplicaciones para tel\IeC {\'e}fonos inteligentes}{49}{chapter.4}
\contentsline {section}{\numberline {4.1}Introducci\IeC {\'o}n}{50}{section.4.1}
\contentsline {section}{\numberline {4.2}Java}{51}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Paradigmas de la programaci\IeC {\'o}n}{51}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Objetos}{52}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}T\IeC {\'e}cnicas de la POO}{57}{subsection.4.2.3}
\contentsline {subsection}{\numberline {4.2.4}Una clase para resolver ecuaciones cuadr\IeC {\'a}ticas}{60}{subsection.4.2.4}
\contentsline {section}{\numberline {4.3}Android}{61}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Importancia de Android como sistema operativo}{61}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Arquitectura de Android}{62}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}Versiones API}{62}{subsection.4.3.3}
\contentsline {subsection}{\numberline {4.3.4}Primer programa en Android Studio}{64}{subsection.4.3.4}
\contentsline {subsection}{\numberline {4.3.5}Componentes de una aplicaci\IeC {\'o}n}{68}{subsection.4.3.5}
\contentsline {subsection}{\numberline {4.3.6}Ciclo de vida de una aplicaci\IeC {\'o}n}{70}{subsection.4.3.6}
\contentsline {subsection}{\numberline {4.3.7}Interfaz de usuario con archivos XML}{71}{subsection.4.3.7}
\contentsline {section}{\numberline {4.4}Interfaz de usuario para resolver la ecuaci\IeC {\'o}n cuadr\IeC {\'a}tica}{72}{section.4.4}
\contentsline {section}{\numberline {4.5}Acoplamiento entre la interfaz de usuario y el programa en Java}{74}{section.4.5}
\contentsline {chapter}{\numberline {5}An\IeC {\'a}lisis y dise\IeC {\~n}o del sistema}{78}{chapter.5}
\contentsline {section}{\numberline {5.1}Plan r\IeC {\'a}pido}{78}{section.5.1}
\contentsline {section}{\numberline {5.2}Modelo de dise\IeC {\~n}o r\IeC {\'a}pido}{81}{section.5.2}
\contentsline {section}{\numberline {5.3} An\IeC {\'a}lisis de un compilador}{81}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Terminolog\IeC {\'\i }a b\IeC {\'a}sica}{82}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}Analizador L\IeC {\'e}xico}{84}{subsection.5.3.2}
\contentsline {subsection}{\numberline {5.3.3}Analizador sint\IeC {\'a}ctico}{102}{subsection.5.3.3}
\contentsline {subsection}{\numberline {5.3.4}Algoritmo para decidir si una palabra est\IeC {\'a} en un lenguaje $w\in L(G)$}{115}{subsection.5.3.4}
\contentsline {subsection}{\numberline {5.3.5}Ejemplo de gram\IeC {\'a}tica para programar}{116}{subsection.5.3.5}
\contentsline {section}{\numberline {5.4}Interfaz de usuario en XML}{123}{section.5.4}
\contentsline {section}{\numberline {5.5}Pseudoc\IeC {\'o}digo para programar los m\IeC {\'e}todos num\IeC {\'e}ricos a fin de resolver el PVI}{124}{section.5.5}
\contentsline {chapter}{\numberline {6}Dise\IeC {\~n}o de la aplicaci\IeC {\'o}n y su uso }{126}{chapter.6}
\contentsline {section}{\numberline {6.1}Etapas de dise\IeC {\~n}o de la aplicaci\IeC {\'o}n: c\IeC {\'o}digo Java}{126}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}Clase \textit {Methods}}{126}{subsection.6.1.1}
\contentsline {subsection}{\numberline {6.1.2}Clase \textit {p1alex}}{128}{subsection.6.1.2}
\contentsline {subsection}{\numberline {6.1.3}Clase \textit {graficador}}{129}{subsection.6.1.3}
\contentsline {subsection}{\numberline {6.1.4}Clase \textit {principal}}{129}{subsection.6.1.4}
\contentsline {subsection}{\numberline {6.1.5}Algunos ejemplos realizados con las clases}{132}{subsection.6.1.5}
\contentsline {section}{\numberline {6.2}Etapas de dise\IeC {\~n}o de la aplicaci\IeC {\'o}n: archivo XML}{133}{section.6.2}
\contentsline {section}{\numberline {6.3}Etapas de dise\IeC {\~n}o de la aplicaci\IeC {\'o}n: incorporaci\IeC {\'o}n de c\IeC {\'o}digos Android Studio}{139}{section.6.3}
\contentsline {section}{\numberline {6.4}Resultados de la aplicaci\IeC {\'o}n}{154}{section.6.4}
\contentsline {chapter}{Conclusiones}{162}{subfigure.6.4.3.3}
\contentsline {chapter}{Trabajos futuros}{163}{Item.126}
\contentsline {part}{Ap\'{e}ndice}{164}{Item.135}
\contentsline {chapter}{\numberline {A}Instalaci\IeC {\'o}n de programas}{165}{appendix.Alph1}
\contentsline {section}{\numberline {A.1}Instalar JDK para ejecutar c\IeC {\'o}digo en lenguaje Java}{165}{section.Alph1.1}
\contentsline {section}{\numberline {A.2}Instalaci\IeC {\'o}n de Android Studio}{165}{section.Alph1.2}
\contentsline {chapter}{\numberline {B}Teoremas}{166}{appendix.Alph2}
\contentsline {subsection}{\numberline {B.0.1}Teorema del valor medio}{166}{subsection.Alph2.0.1}
\contentsline {subsection}{\numberline {B.0.2}Teorema del valor medio del c\IeC {\'a}lculo integral}{166}{subsection.Alph2.0.2}
\contentsline {subsection}{\numberline {B.0.3}Teorema de Taylor}{166}{subsection.Alph2.0.3}
\contentsline {subsection}{\numberline {B.0.4}Cero estabilidad}{167}{subsection.Alph2.0.4}
\contentsline {subsection}{\numberline {B.0.5}Teorema de Picard }{167}{subsection.Alph2.0.5}
\contentsline {chapter}{Bibliograf\IeC {\'\i }a}{167}{equation.Alph2.0.7}
