class ecCuad
{
   private static double a,b,c;
   
   public ecCuad(double a1, double b1, double c1){
     a=a1; b=b1; c=c1;
   }

   public static String  resultado(){
     String sol=" ";
     double sol1=0, sol2=0, r= b*b-4*a*c, aux=-b/(2*a);
	
     if(r>0){
       sol1=(-b+Math.sqrt(r))/(2*a);
       sol2=(-b-Math.sqrt(r))/(2*a);
       sol= "r1="+String.valueOf(sol1)+"  r2="+String.valueOf(sol2);
     }
     else if(r<0){
       sol1=Math.sqrt(-r)/(2*a);
       if(aux!=0)
         sol="r1="+String.valueOf(aux)+"+("+String.valueOf(sol1)+")i";
         sol=sol+" r2="+String.valueOf(aux)+"-i("+String.valueOf(sol1)+")";
       if(aux==0)
         sol="r1=i("+String.valueOf(sol1)+")";
         sol=sol+" r2=-i("+String.valueOf(sol1)+")";
     }else{
       sol="r="+String.valueOf(-b/(2*a));
     }
     return sol;
   } 

   public static void main(String[] args){
   ecCuad p1 = new ecCuad(1,0,1);
   System.out.println(p1.resultado());
   }
}
