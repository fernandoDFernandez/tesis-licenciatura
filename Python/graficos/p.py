import matplotlib.pyplot as plt

plt.style.use('ggplot')
x = [10  , 20 , 30 , 40 , 50 ]
y = [2010,2011,2013,2014,2015]
z = ["C","C++","R","MATLAB","PYTHON"]


plt.xlabel('Opcion1')
plt.ylabel('Opcion2')
plt.title('World')

plt.plot(x,y,label="Max")

plt.xticks(x,z)
plt.legend(loc="best")
plt.show()
