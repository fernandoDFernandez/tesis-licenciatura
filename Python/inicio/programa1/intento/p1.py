import csv

x=[1,2,3,4,5] 
y=[2,3,4,5,6] 

data=[x,y]

with open('myfile.csv','w') as f:
    out = csv.writer(f, delimiter=',',quoting=csv.QUOTE_ALL)
    out.writerows(zip(*data))
