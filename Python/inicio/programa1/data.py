import sys

def DatosdeEntrada():
  l= []
  n = len(sys.argv)
  if n != 5:
      print "Args: t0 y0 b h"
      #print "Args:",sys.argv[]
      sys.exit(1)

  try:
      t0 = float(sys.argv[1])
      l.append(t0)
  except ValueError:
      print "El primer argumento no es numero"
      sys.exit(2)

  try: 
       y0 = float(sys.argv[2])
       l.append(y0)
  except ValueError:
      print "El segundo argumento no es numero"
      sys.exit(3) 

  try: 
       b = float(sys.argv[3])
       l.append(b)
  except ValueError:
      print "El tercer argumento no es numero"
      sys.exit(4) 

  try: 
       h = float(sys.argv[4])
       l.append(h)
  except ValueError:
      print "El cuarto argumento no es numero"
      sys.exit(5) 
  return(l)
