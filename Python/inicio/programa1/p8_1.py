import p8 as operaciones
import sys

n = len(sys.argv)
if n != 3:
     print "Args: numero1 numero2"
     print "Arg[0]:", sys.argv[0],"Arg[1]", sys.argv[1],"Arg[2]:",sys.argv[2],"Arg:",sys.argv[3]
     sys.exit(1)

try:
   n1 = float(sys.argv[1])
except ValueError:
   print "El primer argumento no es un numero"
   sys.exit(2)

try:
   n2 = float(sys.argv[2])
except ValueError:
   print "El segundo argumento no es un numero"
   sys.exit(3)

r1 = operaciones.suma(n1,n2)
r2 = operaciones.resta(n1,n2)
r3 = operaciones.multiplicacion(n1,n2)
r4 = operaciones.factorial(n1)
print "Suma:",r1,"Resta",r2,"Multiplica", r3,"Factorial",r4
