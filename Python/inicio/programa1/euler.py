import data as Verifica
import math as funcion
import numpy as np
from matplotlib import pyplot as G

def f(x,y):
  return(-5*y)    

def y_exacta(x):
  return(funcion.exp(-5*x))

l = Verifica.DatosdeEntrada()
t0 = l[0]
y0 = l[1]
b  = l[2]
h  = l[3] 
n =int((b-t0)/h)
y_num = np.zeros((n+1,1))
y_real = np.zeros((n+1,1))
t = np.zeros((n+1,1))
y_num[0] =y0
y_real[0] = y_exacta(t[0]);

for i in list(range(0,n+1)):
    t[i] = t0+h*i

for i in list(range(0,n)):
      y_num[i+1] = y_num[i] + h*f(t[i],y_num[i]) 
#      y_real[i+1] = y_exacta(t[i+1])

val=30
#G.plot(t,y_real)
G.plot(t,y_num)
G.xlabel('t',fontsize =val)
G.ylabel('y_num',fontsize=val)
G.xticks(fontsize=val)
G.yticks(fontsize=val) 
G.title('Y(t) para h=1/2',fontsize=val)
G.axhline(0,color="black")
G.axvline(0,color="black")
G.show()

#Salida de archivos
arch = open("grafica.txt",'w')

for i in list(range(0,n+1)):
      arch.write("{} {}\n".format(t[i],y_num[i]))

arch.close()






