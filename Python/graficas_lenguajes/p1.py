#coding: utf-8
import matplotlib.pyplot as plt
import numpy as np

t = [2010,2011,2012,2013,2014,2015,2016,2017,2018,2019,2020]
c_cpp = [10.4,12.4,12.2,8.8,8.2,8.1,8,7.7,6.5,6.2,5.9]
java = [28.6,29.3,27.9,26.9,26.3,25.8,25.5,23.5,22.3,21,19]
matlab = [3,3.2,3.1,3.1,3,3,2.9,2.7,2.3,2,1.79]
python = [6.5,6.5,7.7,8.7,10,10.7,12.1,15,20.3,25.4,29.72]



#plt.plot(t, t, 'r--', t, t**2, 'bs', t, t**3, 'g^')
#plt.title(u"Lenguajes de programación, acorde a su búsqueda en Google")   
plt.xlabel(u"Años") 
plt.ylabel("Porcentaje") 
#plt.ioff()
plt.plot(t,c_cpp,'-ok',label="C/C++", linewidth=0.8)
plt.plot(t,java,'-vk',label="Java", linewidth=0.8)
plt.plot(t,matlab,'-sk',label="Matlab", linewidth=0.8)
plt.plot(t,python,'-xk',label="Python", linewidth=0.8)
#plt.legend(loc="best", bbox_to_anchor=(0.5, 0.5)) 
plt.legend(loc="best", bbox_to_anchor=(0.5, 0., 0.5, 0.5)) 
plt.show()

