\BOOKMARK [0][]{Doc-Start}{Resumen}{}% 1
\BOOKMARK [0][]{chapter.1}{1 Introducci\363n}{}% 2
\BOOKMARK [0][]{chapter.2}{2 El problema de valores iniciales en ecuaciones diferenciales ordinarias}{}% 3
\BOOKMARK [1][]{section.2.1}{2.1 Introducci\363n}{chapter.2}% 4
\BOOKMARK [1][]{section.2.2}{2.2 Teor\355a}{chapter.2}% 5
\BOOKMARK [2][]{subsection.2.2.1}{2.2.1 Ejemplos}{section.2.2}% 6
\BOOKMARK [1][]{section.2.3}{2.3 Problemas bien planteados}{chapter.2}% 7
\BOOKMARK [0][]{chapter.3}{3 M\351todos num\351ricos de un paso}{}% 8
\BOOKMARK [1][]{section.3.1}{3.1 Introducci\363n}{chapter.3}% 9
\BOOKMARK [1][]{section.3.2}{3.2 Error global y error local}{chapter.3}% 10
\BOOKMARK [1][]{section.3.3}{3.3 Propiedades importantes de los m\351todos}{chapter.3}% 11
\BOOKMARK [2][]{subsection.3.3.1}{3.3.1 Criterio para analizar los m\351todos}{section.3.3}% 12
\BOOKMARK [2][]{subsection.3.3.2}{3.3.2 Definiciones}{section.3.3}% 13
\BOOKMARK [1][]{section.3.4}{3.4 M\351todos num\351ricos de un paso}{chapter.3}% 14
\BOOKMARK [2][]{subsection.3.4.1}{3.4.1 Estabilidad Num\351rica}{section.3.4}% 15
\BOOKMARK [2][]{subsection.3.4.2}{3.4.2 A-estabilidad}{section.3.4}% 16
\BOOKMARK [1][]{section.3.5}{3.5 M\351todos de Taylor}{chapter.3}% 17
\BOOKMARK [2][]{subsection.3.5.1}{3.5.1 Un m\351todo de orden dos}{section.3.5}% 18
\BOOKMARK [2][]{subsection.3.5.2}{3.5.2 A-Estabilidad }{section.3.5}% 19
\BOOKMARK [1][]{section.3.6}{3.6 M\351todos de Euler}{chapter.3}% 20
\BOOKMARK [2][]{subsection.3.6.1}{3.6.1 M\351todo de Euler expl\355cito}{section.3.6}% 21
\BOOKMARK [2][]{subsection.3.6.2}{3.6.2 M\351todo de Euler impl\355cito}{section.3.6}% 22
\BOOKMARK [1][]{section.3.7}{3.7 M\351todos de Runge-Kutta}{chapter.3}% 23
\BOOKMARK [2][]{subsection.3.7.1}{3.7.1 Un m\351todo de RK de orden dos}{section.3.7}% 24
\BOOKMARK [2][]{subsection.3.7.2}{3.7.2 Cero estabilidad}{section.3.7}% 25
\BOOKMARK [0][]{chapter.4}{4 Desarrollo de aplicaciones para tel\351fonos inteligentes}{}% 26
\BOOKMARK [1][]{section.4.1}{4.1 Introducci\363n}{chapter.4}% 27
\BOOKMARK [1][]{section.4.2}{4.2 Java}{chapter.4}% 28
\BOOKMARK [2][]{subsection.4.2.1}{4.2.1 Paradigmas de la programaci\363n}{section.4.2}% 29
\BOOKMARK [2][]{subsection.4.2.2}{4.2.2 Objetos}{section.4.2}% 30
\BOOKMARK [2][]{subsection.4.2.3}{4.2.3 T\351cnicas de la POO}{section.4.2}% 31
\BOOKMARK [2][]{subsection.4.2.4}{4.2.4 Una clase para resolver ecuaciones cuadr\341ticas}{section.4.2}% 32
\BOOKMARK [1][]{section.4.3}{4.3 Android}{chapter.4}% 33
\BOOKMARK [2][]{subsection.4.3.1}{4.3.1 Importancia de Android como sistema operativo}{section.4.3}% 34
\BOOKMARK [2][]{subsection.4.3.2}{4.3.2 Arquitectura de Android}{section.4.3}% 35
\BOOKMARK [2][]{subsection.4.3.3}{4.3.3 Versiones API}{section.4.3}% 36
\BOOKMARK [2][]{subsection.4.3.4}{4.3.4 Primer programa en Android Studio}{section.4.3}% 37
\BOOKMARK [2][]{subsection.4.3.5}{4.3.5 Componentes de una aplicaci\363n}{section.4.3}% 38
\BOOKMARK [2][]{subsection.4.3.6}{4.3.6 Ciclo de vida de una aplicaci\363n}{section.4.3}% 39
\BOOKMARK [2][]{subsection.4.3.7}{4.3.7 Interfaz de usuario con archivos XML}{section.4.3}% 40
\BOOKMARK [1][]{section.4.4}{4.4 Interfaz de usuario para resolver la ecuaci\363n cuadr\341tica}{chapter.4}% 41
\BOOKMARK [1][]{section.4.5}{4.5 Acoplamiento entre la interfaz de usuario y el programa en Java}{chapter.4}% 42
\BOOKMARK [0][]{chapter.5}{5 An\341lisis y dise\361o del sistema}{}% 43
\BOOKMARK [1][]{section.5.1}{5.1 Plan r\341pido}{chapter.5}% 44
\BOOKMARK [1][]{section.5.2}{5.2 Modelo de dise\361o r\341pido}{chapter.5}% 45
\BOOKMARK [1][]{section.5.3}{5.3 \040An\341lisis de un compilador}{chapter.5}% 46
\BOOKMARK [2][]{subsection.5.3.1}{5.3.1 Terminolog\355a b\341sica}{section.5.3}% 47
\BOOKMARK [2][]{subsection.5.3.2}{5.3.2 Analizador L\351xico}{section.5.3}% 48
\BOOKMARK [2][]{subsection.5.3.3}{5.3.3 Analizador sint\341ctico}{section.5.3}% 49
\BOOKMARK [2][]{subsection.5.3.4}{5.3.4 Algoritmo para decidir si una palabra est\341 en un lenguaje wL\(G\)}{section.5.3}% 50
\BOOKMARK [2][]{subsection.5.3.5}{5.3.5 Ejemplo de gram\341tica para programar}{section.5.3}% 51
\BOOKMARK [1][]{section.5.4}{5.4 Interfaz de usuario en XML}{chapter.5}% 52
\BOOKMARK [1][]{section.5.5}{5.5 Pseudoc\363digo para programar los m\351todos num\351ricos a fin de resolver el PVI}{chapter.5}% 53
\BOOKMARK [0][]{chapter.6}{6 Dise\361o de la aplicaci\363n y su uso }{}% 54
\BOOKMARK [1][]{section.6.1}{6.1 Etapas de dise\361o de la aplicaci\363n: c\363digo Java}{chapter.6}% 55
\BOOKMARK [2][]{subsection.6.1.1}{6.1.1 Clase Methods}{section.6.1}% 56
\BOOKMARK [2][]{subsection.6.1.2}{6.1.2 Clase p1alex}{section.6.1}% 57
\BOOKMARK [2][]{subsection.6.1.3}{6.1.3 Clase graficador}{section.6.1}% 58
\BOOKMARK [2][]{subsection.6.1.4}{6.1.4 Clase principal}{section.6.1}% 59
\BOOKMARK [2][]{subsection.6.1.5}{6.1.5 Algunos ejemplos realizados con las clases}{section.6.1}% 60
\BOOKMARK [1][]{section.6.2}{6.2 Etapas de dise\361o de la aplicaci\363n: archivo XML}{chapter.6}% 61
\BOOKMARK [1][]{section.6.3}{6.3 Etapas de dise\361o de la aplicaci\363n: incorporaci\363n de c\363digos Android Studio}{chapter.6}% 62
\BOOKMARK [1][]{section.6.4}{6.4 Resultados de la aplicaci\363n}{chapter.6}% 63
\BOOKMARK [0][]{subfigure.6.4.3.3}{Conclusiones}{}% 64
\BOOKMARK [0][]{Item.126}{Trabajos futuros}{}% 65
\BOOKMARK [-1][]{Item.135}{Ap\351ndice}{}% 66
\BOOKMARK [0][]{appendix.Alph1}{A Instalaci\363n de programas}{Item.135}% 67
\BOOKMARK [1][]{section.Alph1.1}{A.1 Instalar JDK para ejecutar c\363digo en lenguaje Java}{appendix.Alph1}% 68
\BOOKMARK [1][]{section.Alph1.2}{A.2 Instalaci\363n de Android Studio}{appendix.Alph1}% 69
\BOOKMARK [0][]{appendix.Alph2}{B Teoremas}{Item.135}% 70
\BOOKMARK [1][]{subsection.Alph2.0.1}{B.0.1 Teorema del valor medio}{appendix.Alph2}% 71
\BOOKMARK [2][]{subsection.Alph2.0.2}{B.0.2 Teorema del valor medio del c\341lculo integral}{subsection.Alph2.0.1}% 72
\BOOKMARK [2][]{subsection.Alph2.0.3}{B.0.3 Teorema de Taylor}{subsection.Alph2.0.1}% 73
\BOOKMARK [2][]{subsection.Alph2.0.4}{B.0.4 Cero estabilidad}{subsection.Alph2.0.1}% 74
\BOOKMARK [2][]{subsection.Alph2.0.5}{B.0.5 Teorema de Picard }{subsection.Alph2.0.1}% 75
\BOOKMARK [0][]{equation.Alph2.0.7}{Bibliograf\355a}{Item.135}% 76
