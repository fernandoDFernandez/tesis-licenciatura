r=1; %cicloide
x0=[pi/3,0];
%x(1)=theta, x(2)=dtheta/dt
f=@(t,x) [x(2);(x(2)^2-9.8/r)*tan(x(1)/2)/2]; 
tspan=[0,10];
[t,x]=ode45(f,tspan,x0);

plot(t,x(:,1))
grid on
xlabel('t')
ylabel('\theta');
title('Péndulo')
%la energía total es constante
en=r^2*(1+cos(x(:,1))).*x(:,2).^2-9.8*r*(3+cos(x(:,1)));
