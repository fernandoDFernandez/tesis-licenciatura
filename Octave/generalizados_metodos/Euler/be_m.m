function [t,y] = be_m(f,t0,y0,t_end,h)
 n=fix((t_end-t0)/h)+1;
 t=linspace(t0,t0+(n-1)*h,n)';
 y=zeros(n,1);
 y(1) = y0;
 i=2;
while i <= n
  y(i) = y(i-1)/(1-h*f(t(i)));
  % + h*feval(f,t(i-1),y(i-1));
  i = i+1;
end
	

end
