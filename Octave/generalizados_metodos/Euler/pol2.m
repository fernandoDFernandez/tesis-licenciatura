function [val] = pol(x)
  val = (((0.2*x-1.1205).*x+1.3425).*x+0.00675);
end
