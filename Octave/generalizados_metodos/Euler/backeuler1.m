function [t,y] = backeuler1(f,t0,y0,t_end,h,tol)
 n=fix((t_end-t0)/h)+1;
 t=linspace(t0,t0+(n-1)*h,n)';
 y=zeros(n,1);
 y(1) = y0;
 i=2;
	while i <= n
 	 yt1 = y(i-1) + h*feval(f,t(i-1),y(i-1));
 	 count = 0;
	 diff = 1;
 	 while and((diff>tol),(count <10)) 
 	   yt2 = y(i-1) + h*feval(f,t(i),yt1);
		 diff = abs(yt2-yt1);
 	   yt1 = yt2;
 	   count = count +1;
 	 end
	 if	count >=10
		disp('Not converging after 10 steeps at t=')
		fprintf('%5.5f, %5.5f\n',t(i),h)
	 end
 	 y(i) = yt2;
 	 i = i+1;
	end

end
