# define fixed axis-ranges
set xrange [-1:1]
set yrange [0:20]
set zrange [-1:1]

# filename and n=number of lines of your data 
filedata = 'euler_time.txt'
n = system(sprintf('cat %s | wc -l', filedata))

do for [j=1:n] {
    set title 'time '.j
    splot filedata u 1:2 every 1:j w  l lw 2, \
          filedata u 1:2 every j:j w  p pt 7 ps 2
}

