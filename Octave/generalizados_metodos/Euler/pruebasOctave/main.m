function [] = main()
	%Define condiciones iniciales
	paso=input("Ingresa el paso: ");

	tiempo = input("Ingresar el tiempo: ");
	t0=0; t_end =tiempo; h = paso;
	y0 = zeros(1,2); y0(1) =30; y0(2)=4;

	%Sistema de ecuaciones diferenciales	
	fun1 =@(t,y_f)[0.4*y_f(1)-0.018*y_f(1)*y_f(2),-0.8*y_f(2)+0.023*y_f(1)*y_f(2)];
	
	tspan=t0:h:t_end;

	%Euler
	[t,y] = eulersys(t0, y0, t_end, h, fun1);

	%Runge-Kutta
	[t1,y1] =ode45(fun1,tspan,[y0(1) y0(2)]);	
	
	euler_time1=[t',y(:,1)];
	euler_space1 = [y(:,1),y(:,2)];

	RK_time2=[t',y1(:,1)];
	RK_space2 = [y1(:,1),y1(:,2)];

	save euler_time.txt euler_time1;
	save euler_space.txt euler_space1;
	
	save RK_time.txt RK_time2;
	save RK_space.txt RK_space2;
end
