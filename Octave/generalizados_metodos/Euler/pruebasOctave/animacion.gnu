set terminal gif animate delay 100
set output 'output.gif'
stats 'op1_time.txt' nooutput
set xrange [-0.5:1.5]
set yrange [-0.5:5.5]

do for [i=1:int(STATS_blocks)] {
   plot 'op1_time.txt' index (i-1) with circles
}

