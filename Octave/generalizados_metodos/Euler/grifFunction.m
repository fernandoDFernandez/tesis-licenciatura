function [val] = grifFunction(t,y)
  val = -8*y-40*(3*exp(-(t/8))-1);
end
