function [val] = extrafun(t,y),
  val = (1-2*t).*y;
end
