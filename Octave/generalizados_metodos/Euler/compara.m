function [y] = compara(t1,t2,y2),
  m = length(t1);
  n = length(t2);
  k = min(m,n);
  y = zeros(1,k);
  if k==m
    j=1;
    for i=1:k
      while t1(i) ~= t2(j)
         j=j+1;
      end
      y(i)=y2(j);
    end
  end
  if k==n
    j=1;
    for i=1:k+1
      while t2(i)~= t1(j)
        j=j+1;
      end
      y(i)=y2(j);
    end
  end

end
