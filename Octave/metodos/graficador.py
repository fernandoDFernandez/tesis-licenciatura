#coding: utf-8

import matplotlib.pyplot as plt
import numpy as np
import math	
def fun(x):
	return math.exp(0.05*(x*x*x*x-7.47*x*x*x+13.425*x*x+0.135*x))

r = np.arange(0, 5.1, 0.1)


#0.5
X_1, Y_1 = [], []
for line in open('Taylor_1.txt', 'r'):
  values = [float(s) for s in line.split()]
  X_1.append(values[0])
  Y_1.append(values[1])

#0.1
X_2, Y_2 = [], []
for line in open('Taylor_2.txt', 'r'):
  values = [float(s) for s in line.split()]
  X_2.append(values[0])
  Y_2.append(values[1])


#0.05
X_3, Y_3 = [], []
for line in open('Taylor_3.txt', 'r'):
  values = [float(s) for s in line.split()]
  X_3.append(values[0])
  Y_3.append(values[1])


#t = [2010,2011,2012,2013,2014,2015,2016,2017,2018,2019,2020]
#c_cpp = [10.4,12.4,12.2,8.8,8.2,8.1,8,7.7,6.5,6.2,5.9]
#java = [28.6,29.3,27.9,26.9,26.3,25.8,25.5,23.5,22.3,21,19]
#matlab = [3,3.2,3.1,3.1,3,3,2.9,2.7,2.3,2,1.79]
#python = [6.5,6.5,7.7,8.7,10,10.7,12.1,15,20.3,25.4,29.72]



plt.xlabel(u"t") 
plt.ylabel("Y") 
plt.plot(X_1,Y_1, '.k',label="h=0.5",markersize=10)
#plt.plot(X_2,Y_2,linestyle='-.', marker='x', color='k',label="0.1", linewidth=0.8)
plt.plot(X_2,Y_2,'x',color='#808080',label="h=0.1",markersize=10)
plt.plot(X_3,Y_3,'1k',label="h=0.05",markersize=10)
plt.plot(r,[fun(i) for i in r],'-k',label="Y(t)", linewidth=0.8)
#plt.plot(X_2,Y_2,'-xk',label="0.1", linewidth=0.8)
#plt.plot(X_3,Y_3,'-+k',label="0.05", linewidth=0.8)
#plt.plot(t,python,'-xk',label="Python", linewidth=0.8)
plt.legend(loc='upper left',prop={"size":15}) 
plt.show()

