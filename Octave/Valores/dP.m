function [val] = dP(t),
  val = 0.6*t.^2-2.241*t+1.12005;
end
