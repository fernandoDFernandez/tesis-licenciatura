import numpy as np
import math as funcion
from matplotlib import pyplot as G

def P(t):
   return(0.2*(t**3)-1.1205*(t**2)+1.3425*t+0.00675)

def dP(t):
   return(0.6*(t**2)-2.241*t+1.12005)

def ddP(t):
   return(1.2*t-2.241)

tam_vector=50
t = np.zeros((tam_vector+1,1))
y = np.zeros((tam_vector+1,1))
h = 0.1

for i in list(range(0,tam_vector+1)):
         t[i] = h*i
         y[i] = dP(t[i])+(P(t[i]))**2

val = 20
G.plot(t,y)
G.xlabel('t',fontsize =val)
G.ylabel('V(t)',fontsize=val)
G.xticks(fontsize=val)
G.yticks(fontsize=val)
#G.title('V(t)',fontsize=val)
G.axhline(0,color="black")
G.axvline(0,color="black")
G.show()


