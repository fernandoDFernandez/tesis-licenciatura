cap1.tex  ->  Introduccion
cap2.tex  ->  El PVI en ecuaciones diferenciales ordinarias 
cap3.tex  ->  Métodos numéricos de un paso
cap4.tex  ->  Desarrollo para teléfonos inteligentes. //de aplicaciones en celulares
cap5.tex  ->  Análisis y diseño del sistema
cap6.tex  ->  Diseño de la aplicación y su uso  
cap7.tex  ->  Conclusiones y trabajos futuros
