public class cuad{
    private double a, b, c;
    private String status;
    public cuad(double a, double b, double c){
        this.a = a; this.b = b; this.c = c;
    }
    public String[] resultado(){
        String vval[] = new String[3];
        String val1 = " ", val2 = " ";
        double r = -b/(2*a), k= c/(1.0*a), term = r*r-k;
        if(Math.abs(term)<=1E-16){
            status = "Las soluciones son iguales";
            val1=Double.toString(r);
            val2=Double.toString(r);
        }
        else if(term>0){
            status = "Las soluciones son diferentes";
            double x1 = r+Math.sqrt(term), x2 = r-Math.sqrt(term);
            val1 =Double.toString(x1);
            val2 =Double.toString(x2);
        }
        else{
            double xi = Math.sqrt(-term);
            status = "Soluciuones complejas diferentes";
            val1 =Double.toString(r)+"+ i"+Double.toString(xi);
            val2 =Double.toString(r)+"- i"+Double.toString(xi);
        }
        vval[0] = status; vval[1] = val1; vval[2] = val2;
        return vval;
    }
}
