public class Grafico2 extends AppCompatActivity {


    LineChart linechart;
    String a, b, c, d, e, f, g, h, i, p, date, metodoNum,typeInitValueProblem;
    String fun1Extra1,fun2Extra2,fun3Extra3,fun4Extra4,fun5Extra5;
    double ap, bp, cp, dp, ep, fp, gp, hp, ip;
    int pp;

    ArrayList<ILineDataSet> linedata = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grafico2);

        //----------   LECTURA DE VARIABLES PROCEDENTES DE LA LECTURA       -------------
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        a = (String) bundle.get("a");
        b = (String) bundle.get("b");
        c = (String) bundle.get("c");
        d = (String) bundle.get("d");
        e = (String) bundle.get("e");
        f = (String) bundle.get("f");
        g = (String) bundle.get("g");
        h = (String) bundle.get("h");
        i = (String) bundle.get("i");
        p = (String) bundle.get("p");
        date = (String) bundle.get("palabra");
        date = date + ";"; //primeraEcuacion
        //----------- VALORES DE FUNCIONES ---------------
        fun1Extra1 = (String) bundle.get("funExtra1");
        fun2Extra2 = (String) bundle.get("funExtra2");
        fun3Extra3 = (String) bundle.get("funExtra3");
        fun4Extra4 = (String) bundle.get("funExtra4");
        fun5Extra5 = (String) bundle.get("funExtra5");
        fun1Extra1 += ";";
        fun2Extra2 += ";";
        fun3Extra3 += ";";
        fun4Extra4 += ";";
        fun5Extra5 += ";";
        //------------------------------------------------
        metodoNum = (String) bundle.get("method");
        typeInitValueProblem = (String) bundle.get("condicionesIniciales");// tipo de modo para evaluar las condiciones iniciales
        ap = Double.parseDouble(a);
        bp = Double.parseDouble(b);
        cp = Double.parseDouble(c);
        dp = Double.parseDouble(d);
        ep = Double.parseDouble(e);
        fp = Double.parseDouble(f);
        gp = Double.parseDouble(g);
        hp = Double.parseDouble(h);
        ip = Double.parseDouble(i);
        pp = Integer.parseInt(p);


        //Estado de las variables para realizar la operación
        String msg = " "; //estado de la primera ecuacion
        String hdexV = " ",idexV = " ", jdexV = " ",kdexV = " ", ldexV = " ";

        p1alex var1 = new p1alex();                          //  var1  ------------->>>>>>>>>>>>>>>>

        p1alex hdex = new p1alex();
        p1alex idex = new p1alex();
        p1alex jdex = new p1alex();
        p1alex kdex = new p1alex();
        p1alex ldex = new p1alex();

        hdexV = hdex.operando(fun1Extra1);
        idexV = idex.operando(fun2Extra2);
        jdexV = jdex.operando(fun3Extra3);
        kdexV = kdex.operando(fun4Extra4);
        ldexV = ldex.operando(fun5Extra5);
        if (hdexV.equals("SUCCESS")&&idexV.equals("SUCCESS")&&jdexV.equals("SUCCESS")&&kdexV.equals("SUCCESS")&&ldexV.equals("SUCCESS")){
            var1.Funciones(hdex);
            var1.Funciones2(idex);
            var1.Funciones3(jdex);
            var1.Funciones4(kdex);
            var1.Funciones5(ldex);
            msg = var1.operando(date);
        }else if(!hdexV.equals("SUCCESS")){
            Toast.makeText(getApplicationContext(),hdexV,Toast.LENGTH_SHORT).show();
            finish();
        }else if(!idexV.equals("SUCCESS")){
            Toast.makeText(getApplicationContext(),idexV,Toast.LENGTH_SHORT).show();
            finish();
        }else if(!jdexV.equals("SUCCESS")){
            Toast.makeText(getApplicationContext(),jdexV,Toast.LENGTH_SHORT).show();
            finish();
        }else if(!kdexV.equals("SUCCESS")){
            Toast.makeText(getApplicationContext(),kdexV,Toast.LENGTH_SHORT).show();
            finish();
        }else if(!ldexV.equals("SUCCESS")){
            Toast.makeText(getApplicationContext(),ldexV,Toast.LENGTH_SHORT).show();
            finish();
        }

        if (msg.equals("SUCCESS"))
        {
            //*****************       Objeto QUE EMPLEA METODOS NUMERICOS

            Methods met = new Methods();
            met.asignarEcuacionLexica(var1);

            //************DEFINE EJE X *******************************

            double[] sis1 = met.ReturnX(ap, bp, cp, pp);

            double x = 0;
            for (int j = 0; j < sis1.length; j++) {
                sis1[j] = Math.round(sis1[j] * 100) / 100d;
            }
            linechart = (LineChart) findViewById(R.id.Linechart1);
            linechart.setAutoScaleMinMaxEnabled(true);
            ArrayList<String> xAXES = new ArrayList<>();
            for (int i = 0; i < sis1.length; i++) {
                xAXES.add(i, String.valueOf(sis1[i]));
            }
            String[] xaxes = new String[xAXES.size()];
            for (int i = 0; i < xAXES.size(); i++) {
                xaxes[i] = xAXES.get(i).toString();
            }

            //*********** DEFINE EJE Y

            double[] res1 = new double[0];  //definir como un vector
            double[] res2 = new double[0];
            double[] res3 = new double[0];


            if (metodoNum.equals("Euler")){
                if(typeInitValueProblem.equals("TIPO1")) {
                    res1 = met.Eulermethod(ap, bp, cp, pp);
                }
                if(typeInitValueProblem.equals("TIPO2")){
                    res1 = met.Eulermethod(ap, bp, dp, pp);
                    res2 = met.Eulermethod(ap, bp, ep, pp);
                    res3 = met.Eulermethod(ap, bp, fp, pp);
                }
                if(typeInitValueProblem.equals("TIPO3")){
                    res1 = met.Eulermethod(ap, bp, gp, pp);
                    res2 = met.Eulermethod(ap, bp, hp, pp);
                    res3 = met.Eulermethod(ap, bp, ip, pp);
                }
            }
            else if (metodoNum.equals("RK2")) {
                if(typeInitValueProblem.equals("TIPO1")) {
                    res1 = met.RKord2a1(ap, bp, cp, pp);
                }
                if(typeInitValueProblem.equals("TIPO2")){
                    res1 = met.RKord2a1(ap, bp, dp, pp);
                    res2 = met.RKord2a1(ap, bp, ep, pp);
                    res3 = met.RKord2a1(ap, bp, fp, pp);
                }
                if(typeInitValueProblem.equals("TIPO3")){
                    res1 = met.RKord2a1(ap, bp, gp, pp);
                    res2 = met.RKord2a1(ap, bp, hp, pp);
                    res3 = met.RKord2a1(ap, bp, ip, pp);
                }
            } else if (metodoNum.equals("RK4")) {
                if(typeInitValueProblem.equals("TIPO1")) {
                    res1 = met.RKord4(ap, bp, cp, pp);
                }
                if(typeInitValueProblem.equals("TIPO2")){
                    res1 = met.RKord4(ap, bp, dp, pp);
                    res2 = met.RKord4(ap, bp, ep, pp);
                    res3 = met.RKord4(ap, bp, fp, pp);
                }
                if(typeInitValueProblem.equals("TIPO3")){
                    res1 = met.RKord4(ap, bp, gp, pp);
                    res2 = met.RKord4(ap, bp, hp, pp);
                    res3 = met.RKord4(ap, bp, ip, pp);
                }
            }


            ArrayList<Entry> yAXESfun1 = new ArrayList<>();
            ArrayList<Entry> yAXESfun2 = new ArrayList<>();
            ArrayList<Entry> yAXESfun3 = new ArrayList<>();

            //ArrayList<Entry> yAXEScos = new ArrayList<>();
            for (int i = 0; i < sis1.length; i++) {
                yAXESfun1.add(new Entry((float) res1[i], i));
                if(typeInitValueProblem.equals("TIPO2")||typeInitValueProblem.equals("TIPO3")){
                    yAXESfun2.add(new Entry((float) res2[i], i));
                    yAXESfun3.add(new Entry((float) res3[i], i));
                }
            }

            LineDataSet linedata1 = new LineDataSet(yAXESfun1, date);
            linedata1.setDrawCircles(false);
            linedata1.setColor(Color.BLACK);
            linedata1.setDrawValues(false);    //ESTA OPCION NO PERMITE VISUALIZAR LOS DATOS DEL EJE Y

            String valor_numero=" ";
            String valor_numero2 =" ";
            if(typeInitValueProblem.equals("TIPO2")||typeInitValueProblem.equals("TIPO3")){
                if(typeInitValueProblem.equals("TIPO2")){
                     valor_numero = Double.toString(ep);
                     valor_numero2 = Double.toString(fp);
                }else{
                     valor_numero =  Double.toString(hp);
                     valor_numero2 =  Double.toString(ip);
                }
                LineDataSet linedata2 = new LineDataSet(yAXESfun2,valor_numero);
                linedata2.setDrawCircles(false);
                linedata2.setColor(Color.BLUE);
                linedata2.setDrawValues(false);
                linedata.add(linedata2);
                LineDataSet linedata3 = new LineDataSet(yAXESfun3,valor_numero2);
                linedata3.setDrawCircles(false);
                linedata3.setColor(Color.GREEN);
                linedata3.setDrawValues(false);
                linedata.add(linedata3);
            }

            //LineDataSet linedata2 = new LineDataSet(yAXEScos,"cos");
            //linedata2.setDrawCircles(false);
            //linedata2.setColor(Color.BLUE);

            linedata.add(linedata1);

            //linedata.add(linedata2);

            //****************** SE INCORPORA EL SISTEMA  ******************************

            linechart.setData(new LineData(xaxes, linedata));
            float val = (float) pp + 10;
            linechart.setVisibleXRangeMaximum(val);

            //***************
        }else {
            Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_SHORT).show();
            finish();
        }
    }
}

