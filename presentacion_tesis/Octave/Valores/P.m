function [val] = P(t),
 val = 0.2*t.^3-1.1205*t.^2+1.3425*t+0.00675;
end
