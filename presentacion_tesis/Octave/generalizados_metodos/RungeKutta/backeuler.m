function [t,y] = backeuler(f,t0,y0,t_end,h)
 n=fix((t_end-t0)/h)+1;
 t=linspace(t0,t0+(n-1)*h,n)';
 y=zeros(n,1);
 y(1) = y0;
 i=2;
while i <= n
  yt1 = y(i-1) + h*feval(f,t(i-1),y(i-1));
  count = 0;
  while count <10 
    yt2 = y(i-1) + h*feval(f,t(i),yt1);
    yt1 = yt2;
    count = count +1;
  end
  y(i) = yt2;
  i = i+1;
end

end
