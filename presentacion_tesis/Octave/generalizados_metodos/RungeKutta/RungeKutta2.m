function [t,y] = RungeKutta2(f,t0,y0,t_end,h)
 n = fix((t_end-t0)/h)+1;
 t = linspace(t0,t0+(n-1)*h,n)';
 y = zeros(n,1);
 y(1) = y0;

 for i = 2:n
   k1 = y(i-1);
   k2 = y(i-1)+h*f(t(i-1),k1);
    l = (1/2)*(f(t(i-1),k1)+f(t(i),k2));
   y(i) = y(i-1)+h*l;
 end

end
