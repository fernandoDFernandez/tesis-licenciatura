# calculate the number of points
stats 'euler_time.txt' using 1:2 nooutput

# if you want to have a fixed range for all plots
set xrange [STATS_min_x:STATS_max_x]
set yrange [STATS_min_y:STATS_max_y]

set terminal pngcairo size 100,100
outtmpl = 'output%07d.png'

do for [i=0:STATS_records-1] {
    set output sprintf(outtmpl, i)
    plot 'euler_time.txt' every ::::i with lines title sprintf('n = %d', i)
}
set output
