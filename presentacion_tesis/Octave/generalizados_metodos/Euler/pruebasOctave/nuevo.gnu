set style line 1 \
    linecolor rgb '#0060ad' \
    linetype 1 linewidth 0.5 \
    pointtype 7 pointsize 0.2

set style line 2 \
    linecolor rgb '#8B0000' \
    linetype 1 linewidth 0.7 \
    pointtype 7 pointsize 0.5

set title "Solución numérica" font ",20"
set key left box
plot 'euler_time.txt' with linespoints linestyle 2 title "Euler",'RK_time.txt' with linespoints linestyle 1 title "Runge-Kutta"
pause 10
plot 'euler_space.txt' with linespoints linestyle 2 title "Euler",'RK_space.txt' with linespoints linestyle 1 title "Runge-Kutta"

pause -1


