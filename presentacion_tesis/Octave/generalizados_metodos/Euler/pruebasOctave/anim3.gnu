# calculate the number of points
stats 'euler_time.txt' using 1:2 nooutput

# if you want to have a fixed range for all plots
set xrange [0:100]
set yrange [0:100]

set terminal pngcairo size 800,400
outtmpl = 'output%07d.png'

do for [i=0:1000-1] {
    set output sprintf(outtmpl, i)
    plot 'euler_time.txt' every ::::i with lines title sprintf('n = %d', i)
}
set output
