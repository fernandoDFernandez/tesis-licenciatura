function [] = main_taylor(),
format long;
  [t0,y0] = Taylor(0,1,5,1);
  [t1,y1] = Taylor(0,1,5,0.5);
  [t2,y2] = Taylor(0,1,5,0.1);
  [t3,y3] = Taylor(0,1,5,0.01);
   
  M1 = [t1,y1];
  M2 = [t2,y2];
  M3 = [t3,y3];
  
  %LIMITAR RESULTADOS 
 
  y_1 = compara(t0,t1,y1)';
  y_2 = compara(t0,t2,y2)';
  y_3 = compara(t0,t3,y3)';
  y_real = f1(t0);

    disp("  |    t   |   y     |   y1    |   y2    |   y3    |   y_real  ");
 
  [t0,y0,y_1,y_2,y_3,y_real]

  y_0 = abs(y0-y_real);
  y_1 = abs(y_1-y_real);
  y_2 = abs(y_2-y_real);
  y_3 = abs(y_3-y_real);
  disp("  |    t   | E y     | E y1    | E y2    | E y3    | E y_real  ");
  [t0,y_0,y_1,y_2,y_3,y_real]

  y_real = f1(t3);
  plot(t0,y0); hold on;
  plot(t1,y1); hold on;
  plot(t2,y2); hold on;
  plot(t3,y3); hold on;
  plot(t3,y_real,"c");

  %save Taylor_1.txt M1
  %save Taylor_2.txt M2
  %save Taylor_3.txt M3

  
end
