function [val]=fun(x),
  val = exp((0.05)*(x.^4-7.47*x.^3+13.425*x.^2+0.135*x));
end
