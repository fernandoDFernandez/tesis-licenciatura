class racional{
   private int x;   
   private int y;   
   
   racional(int n,int s){ 
	x = n; 
	if(s==0){
	    y = 1; 
 	    System.out.println("No se puede definir y=0");
        }
	else{
	    y=s;
	}
   }
   public  int retX()
   { 
	return x; 
   }
   public  int retY()
   { 
	return y; 
   }
   public void modX(int valX)
   {  
	x = valX;  
   }
   public void modY(int valY)
   {  
	y = valY;  
   }
   public void VerRacional(){
       System.out.println(retX()+"/"+retY());
   }
   public static racional Suma(racional p, racional q){
     return (new racional((p.retX()*q.retY())+(q.retX()*p.retY()),q.retY()*p.retY()));
  }

   public static void main(String[] args)
   {
      racional a1 = new racional(1,0);
      a1.VerRacional();
      racional a2 = new racional(3,4);
      a2.VerRacional();
      racional a3 = racional.Suma(a1,a2);
      a3.VerRacional();
   }


}
